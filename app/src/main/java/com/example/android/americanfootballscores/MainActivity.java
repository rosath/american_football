package com.example.android.americanfootballscores;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreTeamA = 0, scoreTeamB = 0;
    TextView txtViewScoreTeamA;
    TextView txtViewScoreTeamB;

    // For save the state
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putInt("scoreA", scoreTeamA);
        savedInstanceState.putInt("scoreB", scoreTeamB);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // For obtain the current values of the variables
        if (savedInstanceState != null) {
            scoreTeamA = savedInstanceState.getInt("scoreA");
            scoreTeamB = savedInstanceState.getInt("scoreB");
        }
            setContentView(R.layout.activity_main);


        //Set Font
        String routeFont = "fonts/dk_crayon_crumble.ttf";
        Typeface font = Typeface.createFromAsset(getAssets(), routeFont);
        txtViewScoreTeamA = (TextView) findViewById(R.id.team_a_score);
        txtViewScoreTeamB = (TextView) findViewById(R.id.team_b_score);
        TextView txtViewTeamA = (TextView) findViewById(R.id.teamA);
        TextView txtViewTeamB = (TextView) findViewById(R.id.teamB);
        Button btnPointOneA = (Button) findViewById(R.id.pointOneA);
        Button btnPointTwoA = (Button) findViewById(R.id.pointTwoA);
        Button btnPointThreeA = (Button) findViewById(R.id.pointThreeA);
        Button btnPointSixA = (Button) findViewById(R.id.pointSixA);
        Button btnPointOneB = (Button) findViewById(R.id.pointOneB);
        Button btnPointTwoB = (Button) findViewById(R.id.pointTwoB);
        Button btnPointThreeB = (Button) findViewById(R.id.pointThreeB);
        Button btnPointSixB = (Button) findViewById(R.id.pointSixB);
        txtViewTeamA.setTypeface(font);
        txtViewScoreTeamA.setTypeface(font);
        txtViewTeamB.setTypeface(font);
        txtViewScoreTeamB.setTypeface(font);
        btnPointOneA.setTypeface(font);
        btnPointTwoA.setTypeface(font);
        btnPointThreeA.setTypeface(font);
        btnPointSixA.setTypeface(font);
        btnPointOneB.setTypeface(font);
        btnPointTwoB.setTypeface(font);
        btnPointThreeB.setTypeface(font);
        btnPointSixB.setTypeface(font);
    }

    // add scores for team A

    public void addOneForTeamA(View view) {
        scoreTeamA++;
        displayForTeamA(scoreTeamA);
    }

    public void addTwoForTeamA(View view) {
        scoreTeamA += 2;
        displayForTeamA(scoreTeamA);
    }

    public void addThreeForTeamA(View view) {
        scoreTeamA += 3;
        displayForTeamA(scoreTeamA);
    }

    public void addSixForTeamA(View view) {
        scoreTeamA += 6;
        displayForTeamA(scoreTeamA);
    }

    // add scores for team B

    public void addOneForTeamB(View view) {
        scoreTeamB++;
        displayForTeamB(scoreTeamB);
    }

    public void addTwoForTeamB(View view) {
        scoreTeamB += 2;
        displayForTeamB(scoreTeamB);
    }

    public void addThreeForTeamB(View view) {
        scoreTeamB += 3;
        displayForTeamB(scoreTeamB);
    }

    public void addSixForTeamB(View view) {
        scoreTeamB += 6;
        displayForTeamB(scoreTeamB);
    }
    
    // Show the scores 

    public void displayForTeamA(int score) {
        txtViewScoreTeamA.setText(String.valueOf(score));
    }

    public void displayForTeamB(int score) {
        txtViewScoreTeamB.setText(String.valueOf(score));
    }

    // Reset the scores 

    public void resetScores(View view) {
        scoreTeamA = 0;
        scoreTeamB = 0;
        displayForTeamA(scoreTeamA);
        displayForTeamB(scoreTeamB);
    }

}
